
#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void makeSound()
    {

    }
};

class Cat : public Animal
{
public:
    void makeSound() override
    {
        cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound() override
    {
        cout << "Woof\n";
    }
};

class Cow : public Animal
{
    void makeSound() override
    {
        cout << "Muu\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();

    for (Animal* a : animals)
        a->makeSound();
}
